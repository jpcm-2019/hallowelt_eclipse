package main;

public class Person {
	
	private String name;
	private int alter;
	private String passwort;
	
	public Person(String name, int alter, String passwort) {
		this.name = name;
		this.passwort = passwort;
		setAlter(alter);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAlter() {
		return alter;
	}
	
	public void setAlter(int alter) {
		if (alter < 0) throw new IllegalArgumentException("Das Alter darf nicht negativ sein!");
		this.alter = alter;
	}
	
	public boolean anmelden (String passwortEingabe) {
		return passwort.equals(passwortEingabe);
	}
	
	public String getInfo () {
		return "Die Person "+ getName() + " ist " + getAlter() + " Jahre alt.";
	}
	
	public boolean kannLeistungenEinsehen (Person p) {
		return false;
	}
	
	public boolean kannBenoten () {
		return false;
	}
	
	public String getNotenbild() {
		throw new UnsupportedOperationException("Diese Person verf�gt �ber kein Notenbild");
	}
	
	public void addLeistungsnachweis (int note)  {
		throw new UnsupportedOperationException("Diese Person kann nicht benotet werden");
	}

	public boolean removeLeistung(int note) {
		throw new UnsupportedOperationException("Diese Person kann nicht benotet werden");
	}

}
