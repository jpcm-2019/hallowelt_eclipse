package main;

public class Lehrer extends Person {
	
	private String fach;

	public Lehrer(String name, int alter, String passwort, String fach) {
		super(name, alter, passwort);
		this.fach = fach;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}
	
	@Override
	public String getInfo () {
		return super.getInfo() + "\nDieser Lehrer unterrichtet "+ getFach() +".";
	}
	
	@Override
	public boolean kannLeistungenEinsehen (Person p) {
		return true;
	}
	
	@Override
	public boolean kannBenoten () {
		return true;
	}

}
