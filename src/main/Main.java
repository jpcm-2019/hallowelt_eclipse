package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	private static Person angemeldetePerson = null;
	private static ArrayList<Person> personen = new ArrayList<Person>();

	public static void main(String[] args) {
		/*
		 * System.out.println("Hallo Welt"); Schueler moritz = new
		 * Schueler("Moritz",15); moritz.addLeistungsnachweis(2);
		 * moritz.addLeistungsnachweis(3); System.out.println(moritz.removeLeistung(2));
		 * System.out.println(moritz.removeLeistung(4));
		 * System.out.println(moritz.getDurchschnitt());
		 * System.out.println(moritz.getName()); //Person fakePerson = new
		 * Person("fake",-15);
		 */
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		/*
		 * System.out.print("Was ist dein Name? "); try { String name =
		 * input.readLine(); System.out.println("Hallo, " + name + "!"); } catch
		 * (IOException e) { e.printStackTrace(); }
		 */

		personen.add(new Lehrer("L1", 40, "abc", "Mathe"));
		personen.add(new Lehrer("L2", 50, "123", "Deutsch"));
		personen.add(new Schueler("S1", 15, "passwort", 10));
		personen.add(new Schueler("S2", 11, "0000", 5));
		personen.add(new Schueler("S3", 7, "S3", 2));

		while (true) {
			System.out.print("Befehlseingabe> ");
			try {
				String befehl = input.readLine();
				String[] argumente = befehl.split(" ");
				switch (argumente[0]) {
				case "anmelden":
					Person person = holePerson(argumente[1]);
					if (person.anmelden(argumente[2])) {
						angemeldetePerson = person;
						System.out.println("Nun als " + person.getName() + " angemeldet.");
					} else {
						System.err.println("Falsches Passwort");
					}
					break;
				case "abmelden":
					angemeldetePerson = null;
					System.out.println("Abgemeldet");
					break;
				case "info":
					System.out.println(holePerson(argumente[1]).getInfo());
					break;
				case "leistungen":
					Person schueler = holePerson(argumente[1]);
					if (angemeldetePerson != null
						&& angemeldetePerson.kannLeistungenEinsehen(schueler)) {
						System.out.println(schueler.getNotenbild());
					} else {
						System.err
							.println("Die angemeldete Person ist dazu nicht berechtigt.");
					}
					break;

				case "neueNote":
					schueler = holePerson(argumente[1]);
					if (angemeldetePerson != null && angemeldetePerson.kannBenoten()) {
						schueler.addLeistungsnachweis(Integer.parseInt(argumente[2]));
						System.out.println("Note erfolgreich hinzugefügt.");
					} else {
						System.err
							.println("Die angemeldete Person ist dazu nicht berechtigt.");
					}
					break;

				case "loescheNote":
					schueler = holePerson(argumente[1]);
					if (angemeldetePerson != null && angemeldetePerson.kannBenoten()) {
						if (schueler.removeLeistung(Integer.parseInt(argumente[2]))) {
							System.out.println("Note erfolgreich gelöscht.");
						} else {
							System.err.println("Keine solche Note vorhanden.");
						}
					} else {
						System.err
							.println("Die angemeldete Person ist dazu nicht berechtigt.");
					}
					break;

				default:
					System.err.println("Unbekannter Befehl");
					break;

				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				System.err.println("Der angegebene Wert ist keine Zahl.");
			} catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Zu wenig Parameter.");
			} catch (UnsupportedOperationException e) {
				System.err.println(e.getMessage());
			}

		}

	}

	private static Person holePerson(String name) {
		for (Person i : personen) {
			if (i.getName().equals(name)) {
				return i;
			}
		}
		throw new IllegalArgumentException("Person wurde nicht gefunden.");
	}

}
