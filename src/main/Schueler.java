package main;

import java.util.ArrayList;

public class Schueler extends Person {
	
	public int jahrgangsstufe;
	public ArrayList<Integer> leistungsnachweise;

	public Schueler(String name, int alter, String passwort, int jahrgangsstufe) {
		super(name, alter, passwort);
		leistungsnachweise = new ArrayList<Integer>();
		setJahrgangsstufe(jahrgangsstufe);
	}

	public int getJahrgangsstufe() {
		return jahrgangsstufe;
	}

	public void setJahrgangsstufe(int jahrgangsstufe) {
		if (jahrgangsstufe < 1 || jahrgangsstufe > 13) throw new IllegalArgumentException("Ungueltige Jahrgangsstufe.");
		this.jahrgangsstufe = jahrgangsstufe;
	}
	
	@Override
	public void addLeistungsnachweis (int note) {
		if (note < 1 || note > 6) throw new IllegalArgumentException("Ungueltige Note: "  + note);
		leistungsnachweise.add(note);
	}
	
	@Override
	public boolean removeLeistung (int note) {
		if (note < 1 || note > 6) throw new IllegalArgumentException("Ungueltige Note: "  + note);
		return leistungsnachweise.remove((Integer) note);
	}
	
	public double getDurchschnitt () {
		if (leistungsnachweise.size() == 0) throw new java.lang.ArithmeticException ("Noch keine Leistungsnachweise vorhanden");
		int summe = 0;
		for (int note : leistungsnachweise) {
			summe += note;
		}
		return (double) summe / leistungsnachweise.size();
	}
	
	@Override
	public String getInfo () {
		return super.getInfo() + "\nDieser Sch�ler geht in die " + getJahrgangsstufe() + ". Jahrgangsstufe.";
	}
	
	@Override
	public boolean kannLeistungenEinsehen (Person p) {
		return p == this;
	}
	
	@Override
	public String getNotenbild () {
		String leistungentxt = ""; // "1, 2, 3, 4, 5, 6"
		for (int i = 0; i < leistungsnachweise.size(); i++) {
			leistungentxt += leistungsnachweise.get(i);
			if (i != leistungsnachweise.size()-1) {
				leistungentxt += ", ";
			}
		}
		return "Der Sch�ler hat bisher " + leistungsnachweise.size() + " Leistungsnachweis(e) abgelegt:\n" +
				leistungentxt;
	}
	
}
