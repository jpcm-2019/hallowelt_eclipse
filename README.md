* Dies ist unser erstes Java-Projekt in der Eclipse IDE
* Die relevanten Dateien findet ihr im [Main-Package](./src/main)
* Die Anleitung und Übersicht über die Kurseinheiten findet ihr nach wie vor unter https://gitlab.com/jpcm-2019/lunarlander/-/wikis/Lunarlander-Kurseinheiten
* In diesem GitLab-Repository ist auch der gesamte [Projekt-Ordner](./) einsehbar
